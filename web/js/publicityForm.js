/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

jQuery( document ).ready(function($) {
    
    //console.log($('#dropzone1'));
    var avatar_image=$("#publicity-image").val();
    
    if(avatar_image){
        $("#dropzone0").css("background", "url('http://hspace.alscon-clients.com/frontend/web/media/banners/" + avatar_image + "')");
    }
    
    var dropzone  = document.getElementById('dropzone0');

    if(dropzone){
        dropzone.ondrop = function(e){
                e.preventDefault();
                var img = $("#dropzone0").find("img");
                if(img.length != 1){
                        this.className = 'dropzone';
                        dropzone_id = '#dropzone0';
            }
        };

        dropzone.ondragover = function(){
                this.className = 'dropzone dragover';
                return false;
        };

        dropzone.ondragleave = function(){
                this.className = 'dropzone';
                return false;
        };

        $( ".dropzone" ).click(function() {
                var id = $(this).attr('id').slice(-1);
                $('#open_browse' + id).trigger('click');
                dropzone_id = '#'+$(this).attr('id');
        });

        $(".input_image_faile").change( function(event) {
            var id = $(this).attr('id').slice(-1);
            var tmppath = URL.createObjectURL(event.target.files[0]);
            $("#dropzone" + id).css("background", "url('" + URL.createObjectURL(event.target.files[0]) + "')");
            
            var upload_image_path = $(this).val();
            var upload_image_path_array=upload_image_path.split('\\');
            var image_name= upload_image_path_array[upload_image_path_array.length - 1];
            
            $("#publicity-image").val(image_name);
        });

    }
    $('#publicity-position').change(function(e){
        e.preventDefault();
        var value = $(this).val();
        var positions = ['bottom.jpg','top_right.jpg','slider.jpg'];
        if(value!=''){
            var origin_url = $('#position_view').attr('src');
            if(origin_url.length>27){
                var true_url = origin_url.substr(0,27);
            }else {
                var true_url = origin_url;
            }
            var img_position = $('#position_view').attr('src', true_url +'/'+positions[value]);
            $('#position_view').show();
            if(value==0){
                $('#dropzone0').css({ "width": "100%", "height": "72px" });
            }
            if(value==1){
                $('#dropzone0').css({ "width": "165px", "height": "100%" });
            } else{
                $('#dropzone0').css({ "width": "31.4%", "height": "204" });
            }

        }else{
            $('#position_view').hide();
        }

    });
});
